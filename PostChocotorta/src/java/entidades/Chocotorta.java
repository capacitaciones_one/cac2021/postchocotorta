package entidades;

import util.EsNumero;

public class Chocotorta {

    private String nombre;
    private String precio;

    public String calcularIVA() {
        String precio = getPrecio();
        double resultado =0;
        int precioNumber=0;
        String mensaje = "";

        boolean esOnoEs = EsNumero.isNumeric(precio);

        if (esOnoEs == true) {
            precioNumber = Integer.parseUnsignedInt(precio);
            resultado = precioNumber * 0.21;
            //mensaje = "";
            System.out.println(mensaje);
        } else {
            mensaje = "NOOO, no es un numero, chabon.";
            setPrecio("0");
            System.out.println(mensaje);
        }

        return mensaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Chocotorta{" + "nombre=" + nombre + ", precio=" + precio + '}';
    }

}
