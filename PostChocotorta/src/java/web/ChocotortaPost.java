package web;

import entidades.Chocotorta;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ChocotortaPost", urlPatterns = {"/ChocotortaPost"})
public class ChocotortaPost extends HttpServlet {

    Gson convertirJson = new Gson();
    ArrayList<Chocotorta> miListado = new ArrayList();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Guarda lo que envio el navegador
        String pedido = req.getReader().readLine();        

        //Creamos Objeto de java desde formato JSON
        Chocotorta guardado = convertirJson.fromJson(pedido, Chocotorta.class);

        // calcular el IVA
        //System.out.println("Precio sin IVA es: " + guardado.getPrecio() + " Precio con IVA es: " + ( guardado.getPrecio() + guardado.calcularIVA()));
        
        String precioMensaje = guardado.calcularIVA();       

        //Da una respuesta al clienta, que se ve en Network/Response
        if(precioMensaje.equals("")){
            resp.getWriter().println("To chocotorta ya la comimos. Thank you, baby.");
                    //Agrega nuevo pedido al listado
            miListado.add(guardado);

        }else{
            resp.getWriter().println(precioMensaje);
        }
        

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().println(convertirJson.toJson(miListado));
    }

}
